<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license MIT
 * @version 04.01.22 23:49:14
 */

declare(strict_types = 1);
namespace dicr\validate;

use yii\base\InvalidConfigException;

use function gettype;
use function is_scalar;

/**
 * Валидатор почтовых индексов.
 */
class ZipValidator extends AbstractValidator
{
    /** @var int кол-во цифр в индексе (Украина - 5, Россия - 6) */
    public int $digits = 6;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        parent::init();

        if ($this->digits < 1) {
            throw new InvalidConfigException('digits');
        }
    }

    /**
     * @inheritDoc
     */
    public function parseValue(mixed $value): ?int
    {
        if (empty($value)) {
            return null;
        }

        if (! is_scalar($value)) {
            throw new ValidateException('Некорректный тип индекса: ' . gettype($value));
        }

        if (! preg_match('~^\d{1,' . $this->digits . '}$~u', (string)$value)) {
            throw new ValidateException('Некорректное значение индекса: ' . $value);
        }

        $value = (int)$value;

        return $value ?: null;
    }

    /**
     * @inheritDoc
     */
    public function formatValue(mixed $value): string
    {
        $value = $this->parseValue($value);

        return $value === null ? '' : sprintf('%0' . $this->digits . 'd', $value);
    }
}
