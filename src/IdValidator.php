<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license MIT
 * @version 04.01.22 23:53:00
 */

declare(strict_types = 1);
namespace dicr\validate;

/**
 * Валидация ID
 * Значение - целое число > 0
 */
class IdValidator extends AbstractValidator
{
    /**
     * @inheritDoc
     *
     * @param mixed $value
     * @return ?int
     */
    public function parseValue(mixed $value): ?int
    {
        $value = (string)$value;
        if (empty($value)) {
            return null;
        }

        if (! preg_match('~^\d+$~', $value)) {
            throw new ValidateException('Некорректный формат id: ' . $value);
        }

        $value = (int)$value;

        return empty($value) ? null : $value;
    }
}
