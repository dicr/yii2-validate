<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license MIT
 * @version 04.01.22 23:52:11
 */

declare(strict_types = 1);
namespace dicr\validate;

use yii\validators\EmailValidator;

use function implode;
use function is_string;
use function preg_split;

use const PREG_SPLIT_NO_EMPTY;

/**
 * Валидатор списка E-Mail адресов в формате сроки через пробел или запятую.
 */
class EmailsValidator extends AbstractValidator
{
    /** @var string разделитель email */
    public const SPLIT_REGEX = '~\s*[,;]\s*~u';

    /** @var bool проверка домена в DNS */
    public bool $checkDNS = true;

    /** @var bool поддержка IDN */
    public bool $enableIDN = true;

    /**
     * @inheritDoc
     *
     * @return string[]|null список email
     */
    public function parseValue(mixed $value): ?array
    {
        if ($value === null || $value === '' || $value === 0 || $value === '0' || $value === []) {
            return null;
        }

        if (is_string($value)) {
            $value = (array)preg_split(self::SPLIT_REGEX, $value, -1, PREG_SPLIT_NO_EMPTY);
        }

        $ret = [];
        $emailValidator = $this->emailValidator();

        foreach ((array)$value as $val) {
            /** @var ?string $error */
            $error = null;
            if ($emailValidator->validate($val, $error)) {
                $ret[] = $val;
            } else {
                throw new ValidateException($error);
            }
        }

        return $ret ?: null;
    }

    /**
     * @inheritDoc
     */
    public function filterValue(mixed $value): array
    {
        if ($value === null || $value === '' || $value === 0 || $value === '0' || $value === []) {
            return [];
        }

        if (is_string($value)) {
            $value = (array)preg_split(self::SPLIT_REGEX, $value, -1, PREG_SPLIT_NO_EMPTY);
        }

        $ret = [];
        $emailValidator = $this->emailValidator();

        foreach ((array)$value as $val) {
            if ($emailValidator->validate($val, $error)) {
                $ret[] = $val;
            }
        }

        return $ret;
    }

    /**
     * @inheritDoc
     */
    public function formatValue(mixed $value): string
    {
        $value = $this->parseValue($value);

        return empty($value) ? '' : implode(', ', $value);
    }

    private EmailValidator $_emailValidator;

    /**
     * Валидатор E-mail
     */
    private function emailValidator(): EmailValidator
    {
        if (! isset($this->_emailValidator)) {
            $this->_emailValidator = new EmailValidator([
                'skipOnEmpty' => true,
                'checkDNS' => $this->checkDNS,
                'enableIDN' => $this->enableIDN
            ]);
        }

        return $this->_emailValidator;
    }
}
